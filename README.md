# hrd

## Project setup
```
npm install
Rename file .env.staging.local.example jadi .env.staging.local
Ubah nilai VUE_APP_API_URL dan VUE_APP_MAPBOX_API
```

### Compiles and hot-reloads for development
```
npm run staging
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
