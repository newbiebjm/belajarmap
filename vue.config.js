const webpack = require('webpack')

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        mapboxgl: 'mapbox-gl',
        mapboxGeocoder: 'vue-mapbox-geocoder',
        mapboxDirection: 'vue-mapbox-direction',
        axios: 'axios'
      }),
    ],
  },
  // chainWebpack: config => {
  //   config.plugin('optimize-css').tap(([options]) => {
  //     options.cssnanoOptions.preset[1].svgo = false
  //     return [options]
  //   })
  // }
}