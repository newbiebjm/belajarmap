const state = {}

const getters = {}

const actions = {
  authLogin({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/auth/login`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
}

const mutations = {}

export default {
  state,
  getters,
  actions,
  mutations
}
