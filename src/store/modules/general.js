const state = {
  loading: false
}

const getters = {
  getLoading : (state) => state.loading
}

const actions = {}

const mutations = {
  setLoading: (state, value) => (state.loading = value)
}

export default {
  state,
  getters,
  actions,
  mutations
}
