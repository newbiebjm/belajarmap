const state = {
  totalGisPoint: 0,
  totalGisPointActive: 0,
  totalGisPointInactive: 0,
  totalGisPointType: 0,
  gisPoints: [],
  gistPointTypes: [],
  gisFeatureCollections: [
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [-77.03238901390978, 38.913188059745586],
      },
      properties: {
        title: 'SMPN 2 Tamansari Bogor',
        icon: 'monument',
        image: 'https://via.placeholder.com/768'
      },
    }
  ],
  gisHistoryFeatureCollections: []
}

const getters = {
  getGisPoints : (state) => state.gisPoints,
  getGistPointTypes : (state) => state.gistPointTypes,
  getTotalGisPoint : (state) => state.totalGisPoint,
  getTotalGisPointActive : (state) => state.totalGisPointActive,
  getTotalGisPointInactive : (state) => state.totalGisPointInactive,
  getTotalGisPointType : (state) => state.totalGisPointType,
  getGisFeatureCollections : (state) => state.gisFeatureCollections,
  getGisHistoryFeatureCollections : (state) => state.gisHistoryFeatureCollections,
}

const actions = {
  async fetchTotalGisPoint({commit}) {
    await axios.get(`${process.env.VUE_APP_API_URL}/api/gis_point/total`)
      .then(res => {
        const { data } = res
        commit('setTotalGisPoint', data.data)
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchTotalGisPointActive({commit}) {
    await axios.get(`${process.env.VUE_APP_API_URL}/api/gis_point/total?status=1`)
      .then(res => {
        const { data } = res
        commit('setTotalGisPointActive', data.data)
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchTotalGisPointInactive({commit}) {
    await axios.get(`${process.env.VUE_APP_API_URL}/api/gis_point/total?status=0`)
      .then(res => {
        const { data } = res
        commit('setTotalGisPointInactive', data.data)
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchTotalGisPointType({commit}) {
    await axios.get(`${process.env.VUE_APP_API_URL}/api/point_type/total`)
      .then(res => {
        const { data } = res
        commit('setTotalGisPointType', data.data)
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchGistPointTypes({commit}) {
    await axios.get(`${process.env.VUE_APP_API_URL}/api/point_type/all?per_page=100`)
      .then(res => {
        const { data } = res
        commit('setGistPointTypes', data.data)
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchGisPointFeatureCollections({commit}) {
    return await axios.get(`${process.env.VUE_APP_API_URL}/api/gis_point/featurecollections`)
      .then(res => {
        const { data } = res
        commit('setGisFeatureCollections', data.data)
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  async fetchGisPointHistoryFeatureCollections({commit}) {
    return await axios.get(`${process.env.VUE_APP_API_URL}/api/gis_point_history/featurecollections`)
      .then(res => {
        const { data } = res
        commit('setGisHistoryFeatureCollections', data.data)
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  saveGisPoint({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/gis_point/create`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  updateGisPoint({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/gis_point/update`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  deleteGisPoint({commit}, id) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/gis_point/delete/` + id,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  saveGisPointHistory({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/gis_point_history/create`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  saveGisPointType({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/point_type/create`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  },
  updateGisPointType({commit}, data) {
    return axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API_URL}/api/point_type/update`,
      data: data,
      config: {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
      })
      .then(res => {
        return res;
      }).catch(err => {
        console.log(err)
      })
  }
}

const mutations = {
  setGisPoints: (state, value) => (state.gisPoints = value),
  setGistPointTypes: (state, value) => (state.gistPointTypes = value),
  setTotalGisPoint: (state, value) => (state.totalGisPoint = value),
  setTotalGisPointActive: (state, value) => (state.totalGisPointActive = value),
  setTotalGisPointInactive: (state, value) => (state.totalGisPointInactive = value),
  setTotalGisPointType: (state, value) => (state.totalGisPointType = value),
  setGisFeatureCollections: (state, value) => (state.gisFeatureCollections = value),
  setGisHistoryFeatureCollections: (state, value) => (state.gisHistoryFeatureCollections = value),
}

export default {
  state,
  getters,
  actions,
  mutations
}
