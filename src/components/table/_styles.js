const styles =  {
  table: {
      tableClass: 'table table-hover table-striped mb-0',
          ascendingIcon: 'fa fa-sort-up',
          descendingIcon: 'fa fa-sort-down',
          sortableIcon: 'fa fa-sort',
  },
}

export default styles