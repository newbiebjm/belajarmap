import Vue from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'

Vue.config.productionTip = false

import BaseHeader from './components/BaseHeader'
import BaseSidebar from './components/BaseSidebar'
import BaseFooter from './components/BaseFooter'
import BasePointType from './components/BasePointType'

Vue.component('base-header', BaseHeader);
Vue.component('base-sidebar', BaseSidebar);
Vue.component('base-footer', BaseFooter);
Vue.component('base-point-type', BasePointType);

import { Plugin } from 'vue-fragment'
Vue.use(Plugin)

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
Vue.use(datePicker);

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import VueSweetalert2 from 'vue-sweetalert2';
 
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2);

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
