import Vue from 'vue'
import Router from 'vue-router'
import modules from './modules'
import store from '../store'

Vue.use(Router)

const router =  new Router({
    routes: modules
});

router.beforeEach((to, from, next) => {
    store.commit('setLoading', true)
    next()
})
router.afterEach((to, from, next) => {
    store.commit('setLoading', false)
});

export default router