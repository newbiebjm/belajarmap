const requireModule = require.context('./modules', false, /\.js$/)
let modules = []

requireModule.keys().forEach(fileName => {
  if(fileName == './index.js') return

  requireModule(fileName).default.forEach(m => {
    modules.push(m)
  })

})

export default modules
