const PengaturanAkunPage = () => import('@/pages/pengaturan/Akun')
const PengaturanUmumPage = () => import('@/pages/pengaturan/Umum')

const routes = [
  {
    path: '/setting/account',
    name: 'PengaturanAkunPage',
    component: PengaturanAkunPage
  },
  {
    path: '/setting/general',
    name: 'PengaturanUmumPage',
    component: PengaturanUmumPage
  }
];

export default routes
  