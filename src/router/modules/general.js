const DashboardPage = () => import('@/pages/dashboard/Dashboard')

const routes = [
  {
    path: '/',
    name: 'DashboardPage',
    component: DashboardPage
  }
];

export default routes
  