const GisPage = () => import('@/pages/gis/Gis')
const GisPointPage = () => import('@/pages/gis/Point')
const GisPointTypePage = () => import('@/pages/gis/Type')
const GisPerjalananPage = () => import('@/pages/gis/Perjalanan')
const GisHistoryPerjalanan = () => import('@/pages/gis/HistoryPerjalanan')

const GisTambahTitikPoint = () => import('@/pages/gis/TambahTitikPoint')
const GisTambahJenisTitikPoint = () => import('@/pages/gis/TambahJenisTitikPoint')

const routes = [
  {
    path: '/apps/gis',
    name: 'GisPage',
    component: GisPage
  },
  {
    path: '/apps/gis/points',
    name: 'GisPointPage',
    component: GisPointPage
  },
  {
    path: '/apps/gis/point/types',
    name: 'GisPointTypePage',
    component: GisPointTypePage
  },
  {
    path: '/apps/gis/directions',
    name: 'GisPerjalananPage',
    component: GisPerjalananPage
  },
  {
    path: '/apps/gis/direction/histories',
    name: 'GisHistoryPerjalanan',
    component: GisHistoryPerjalanan
  },
  {
    path: '/apps/gis/point/create',
    name: 'GisTambahTitikPoint',
    component: GisTambahTitikPoint
  },
  {
    path: '/apps/gis/point/type/create',
    name: 'GisTambahJenisTitikPoint',
    component: GisTambahJenisTitikPoint
  }
];

export default routes
  