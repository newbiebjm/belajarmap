const AuthLoginPage = () => import('@/pages/auth/Login')

const routes = [
  {
    path: '/apps/auth/login',
    name: 'AuthLoginPage',
    component: AuthLoginPage
  }
];

export default routes
  